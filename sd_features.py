# -*- coding: utf-8 -*-
# Copyright (c) Montreal Neurological Institute and Hospital, Anphy lab;
# Institute of Scientific Instruments of the CAS, v. v. i., Medical signals -
# Computational neuroscience lab. All Rights Reserved.
# Distributed under the (new) BSD License.

import numpy as np
from scipy.stats import skew


def sd_features(rates):
    """
    Calculation of spike dynamics features.

    KLIMES, Petr, Laure PETER-DEREX, Jeff HALL, François DUBEAU a Birgit FRAUSCHER.
    Spatio-temporal spike dynamics predict surgical outcome in adult focal epilepsy.
    Clinical neurophysiology: official journal of the International Federation of
    Clinical Neurophysiology. 2022, 134, 88–99. ISSN 1388-2457.

    Parameters
    ----------
    rates: list of numpy arrays
        Each numpy array holds spike rates in EEG channels from x-minute long
        time segment. To get reliable results use at least 108 10-minutes long segments
        of EEG (18 hours). (int, float)

    Returns
    -------
    rates_mean_var: float
        Spike rate variance across EEG channels.
    ll_var: float
        Line length variance across time segments.
    skew_var: float
        Skewness variance across time segments.

    Example
    -------
    # example with spike rates calculated in 3 time segments on 10 EEG channels.
    rates = [np.array([1,3,2,10,12,10,2,3,4,1]),np.array([3,5,4,9,14,10,2,1,2,2]),np.array([1,5,5,11,12,10,2,2,2,1])]
    rates_mean_var, ll_var, skew_var = sd_features(rates)
    """

    skews = []
    lls = []
    for rates_e in rates:
        rates_e_norm = (rates_e - min(rates_e)) / max(rates_e - min(rates_e))
        ll = sum(np.abs(np.subtract(rates_e_norm[1:], rates_e_norm[:-1]))) / len(rates_e_norm)
        sk = skew(rates_e_norm)
        lls.append(ll)
        skews.append(sk)

    rates_mean = np.mean(rates, 0)
    rates_mean_var = np.var(rates_mean)
    ll_var = np.var(lls)
    skew_var = np.var(skews)

    return rates_mean_var, ll_var, skew_var
