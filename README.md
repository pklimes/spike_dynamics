# Spatio-temporal spike dynamics

Publication: KLIMES, Petr, Laure PETER-DEREX, Jeff HALL, François DUBEAU a Birgit FRAUSCHER. Spatio-temporal spike dynamics predict surgical outcome in adult focal epilepsy. Clinical neurophysiology: official journal of the International Federation of Clinical Neurophysiology. 2022, 134, 88–99. ISSN 1388-2457.

Calculation of spike dynamic features from long EEG recordings. Developed by the Montreal Neurological Institute and Hospital, Anphy lab; and Institute of Scientific Instruments of the Czech Academy of Sciences, Computational neuroscience lab.

Feature #1
Spike variance - a high variance reflects higher differences in spike rates across anatomical localizations, caused by a strong and single source, which is relatively stable over time as contrasted to multiple sources with lower spike rates and shifting predominance of sources over time resulting in lower variance values.

Feature #2
Line length variance - a lower line length indicates lower number of distinct sources, whereas higher values are caused by multiple sources. The variance of line length in time is heavily influenced by unstable epileptogenic sources, which are turning “ON and OFF” over time and different anatomical locations. 
Low variance of line length points to more stable sources.

Feature #3
Skewness variance - a lower positive skewness is caused by multiple sources or a “noisy-looking” distribution, whereas a high positive skewness is obtained when only one distinct source is present. The variance of skewness in time is heavily influenced by unstable epileptogenic sources, which are turning “ON and OFF” over time and different anatomical locations. Low variance of skewness points to more stable sources.
